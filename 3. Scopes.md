- We know that JSPs are Servlets actually and Tomcat converts them.
-   All the scope concepts from Servlet can be mapped here too. There are difference in how we access these objects.
-   Let's create a JSP similar to the one in Servlet to just access a username.
-   **request** object is available in JSP, all we have to do is use it to get the parameter.
    ![](Images/89.png)
-   Run it and pass the name as URL parameter
    ![](Images/90.png)
-   We get the **session** and **application**(Same as context in Servlet) objects and print the parameters
    ![](Images/92.png)
-   Run this in eclipse and verify
    ![](Images/91.png)
    ![](Images/93.png)
-   Also run in another browser to check the **application parameter**.
    ![](Images/94.png)
---------------
-   In addition to these 3 objects, there is another object in JSP called **pageContext** this **doesn't have a equivalent in Servlet**
    ```jsp
    if(userName!=null){
        session.setAttribute("sessionName", userName);
        application.setAttribute("applicationName", userName);
        pageContext.setAttribute("pageContextName", userName);
    }
    ```
    ```jsp
    The username in the pageContext object is :<%=pageContext.getAttribute("pageContextName") %>
    ```
-   Run and check this
    ![](Images/95.png)
    ![](Images/96.png)
-   This setAttribute method is **overloaded**, using which we can set the other 3 object's parameters.
    ```jsp
    //application.setAttribute("applicationName", userName);
	pageContext.setAttribute("applicationName", userName, pageContext.APPLICATION_SCOPE);
    ```
-   There is another method **findAttribute()**, which can be used  to find an attribute in any of the objects, returns null if not present
    ```jsp
    pageContext.findAttribute("name");
    ```
    We use it when we don't know in which scope the parameter is present 

