#   JSP
-   Java Server Pages (JSP) is a server-side programming technology that enables the creation of dynamic, platform-independent method for building Web-based applications.
-   JSP are always compiled before they are processed by the server
-   JavaServer Pages are **built on top of the Java Servlets API**, so like Servlets, JSP also has access to all the powerful Enterprise Java APIs, including JDBC, JNDI, EJB, JAXP, etc.

#   Elements of JSP

![](Images/tags.jpg)

##  The Scriptlet
-   A scriptlet can contain any number of JAVA language statements, variable or method declarations, or expressions that are valid in the page scripting language.
-   ```<% code fragment %> ```
-   Any text, HTML tags, or JSP elements you write must be outside the scriptlet.
##  JSP Declarations
-   A declaration declares one or more variables or methods that you can use in Java code later in the JSP file. 
-   You must declare the variable or method before you use it in the JSP file.
-   ```jsp
    <%! int i = 0; %> 
    <%! int a, b, c; %> 
    <%! Circle a = new Circle(2.0); %> 
    ```
##  Expression
-   A JSP expression element contains a scripting language expression that is evaluated, converted to a String, and inserted where the expression appears in the JSP file.
-   The expression element can contain any expression that is valid according to the Java Language Specification but you cannot use a semicolon to end an expression.
-   ```<%= expression %>```

# Directives
-   The jsp directives are messages that tells the web container how to translate a JSP page into the corresponding servlet.
-   There are three types of directives:
    -   page directive
    -   include directive
    -   taglib directive
-   ```<%@ directive attribute="value" %>  ```

#   Scopes
![](https://www.oreilly.com/library/view/web-technology-theory/9789332508194/images/pg241_img01.png)
-   The availability of a JSP object for use from a particular place of the application is defined as the scope of that JSP object. 
-   Every object created in a JSP page will have a scope. 
-   Object scope in JSP is segregated into four parts
    -  page
        -   ‘page’ scope means, the JSP object can be accessed only from within the same page where it was created. 
        -    JSP implicit objects out, exception, response, pageContext, config and page have ‘page’ scope.
    -  request
        -   A JSP object created using the ‘request’ scope can be accessed from any pages that serves that request. 
        -   The JSP object will be bound to the request object.  Implicit object request has the ‘request’ scope.
    -   session
        -   ‘session’ scope means, the JSP object is accessible from pages that belong to the same session from where it was created. 
        -   The JSP object that is created using the session scope is bound to the session object. Implicit object session has the ‘session’ scope.
    -   application
        -   A JSP object created using the ‘application’ scope can be accessed from any pages across the application. 
        -   The JSP object is bound to the application object. Implicit object application has the ‘application’ scope.
-   pageContext
    -   pageContext is an implicit object of type PageContext class.
    -   The pageContext object can be used to set,get or remove attribute from one of the following scopes:

        page
        
        request
            
        session
            
        application
#   Lifecycle
A JSP life cycle is defined as the process from its creation till the destruction. This is similar to a servlet life cycle with an additional step which is required to compile a JSP into servlet.

![](https://www.tutorialspoint.com/jsp/images/jsp_life_cycle.jpg)
##  JSP Compilation
-   When a browser asks for a JSP, the JSP engine first checks to see whether it needs to compile the page. 
-   If the page has never been compiled, or if the JSP has been modified since it was last compiled, the JSP engine compiles the page.
-   The compilation process involves three steps −
    -   Parsing the JSP.
    -   Turning the JSP into a servlet.
    -   Compiling the servlet.
##  JSP Initialization
-   When a container loads a JSP it invokes the jspInit() method before servicing any requests.
-   ```jsp
    public void jspInit(){
        // Initialization code...
    }
    ```
-   Typically, initialization is performed only once and as with the servlet init method, you generally initialize database connections, open files, and create lookup tables.
##  JSP Execution
-   This phase of the JSP life cycle represents all interactions with requests until the JSP is destroyed.
-   Whenever a browser requests a JSP and the page has been loaded and initialized, the JSP engine invokes the _jspService() method in the JSP.
-   The _jspService() method takes an HttpServletRequest and an HttpServletResponse as its parameters 
    ```jsp
    void _jspService(HttpServletRequest request, HttpServletResponse response) {
    // Service handling code...
    }
    ```
##  JSP Cleanup
-   The destruction phase of the JSP life cycle represents when a JSP is being removed from use by a container.
-   The jspDestroy() method is the JSP equivalent of the destroy method for servlets. 
-   Override jspDestroy when you need to perform any cleanup, such as releasing database connections or closing open files.
-   ```jsp
    public void jspDestroy() {
    // Your cleanup code goes here.
    }
    ```