-   What happens if we put the method inside the scriplet
    ```jsp
    <%
		int	add(int i,int j){
			return	i+j;
		}
		int	i=1;
		int	j=3;
		int	k=i+j;
	%> 
    ```
    ![](Images/80.png)
-   It is throwing an exception, saying we cannot have a method definition. Inside scriplet it expects only statements.
-   Make the code same as before
-   Let's open another script tag and write a _for loop_ in it
    ```jsp
	<%
		for(i=0;i<4;i++){
			out.println("I ="+i);
		}
	%>
    ```
-   Check the output

    ![](Images/81.png)
-   Now let us remove the static lines outside the script tag, and close the _for loop_ in another script tag.
    ```jsp
    <%
		for(i=0;i<4;i++){
	%>
		<br>New value of I =<%=i %>
	<%  
		}
	%>
    ```
-   This works same as enclosing all of them under one tag.

    ![](Images/82.png)
-   The way it works is this JSP is actually converted to a Java class by tomcat i.e. a servlet. So all the **JSPs are a servlet eventually**.
    -   So, every code inside a script tag is generated inside a **doGet** or **doPost** in the **servlet**.
    -   Every simple HTML statement are converted into **PrintWriter** statements to print.
    -   Now we understand why the method definition cannot be done inside a script tag as we **cannot define a new method inside a method**, i.e inside the doGet.
    
        So whatever we put in the declaraion tag is separately written as a **new method outside the doGet**
-   These servlets are created by Tomcat in the Tomcat installation folder -> work
    ![](Images/83.png)
-   Let's see how the Servlet looks like.
    ![](Images/84.png)
    -   As Tomcat doesn't know which method we will be using, it's going to put all those statements into **jspService()** which catches both get and post.
    -   We can see all the **html code is converted**.
    -   See how the **for** loop is converted.

#   Directives
-   Create a new jsp file called "Clock"
    ![](Images/85.png)
-   We can see the error, that we have to import.
-   We can't type import inside script tage because it goes inside the method.
-   We put the import statement in the heading i.e the **directive**, compilation error isn't seen now.
    ```jsp
    <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" import="java.util.Date"%>
    ```
-   Run this
    ![](Images/86.png)
-   Every JSP file can use a **Page directive** and specify properties applicable throughout the page.
-   Go through each property.
-   In case there are lot of properties to be added, all we can do is split it up.
    ```jsp
    <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
    <%@page import="java.util.Date"%>
    ```
##  include
-   Create another simple JSP with just a simple text.
    ![](Images/87.png)
-   To get this utility to another JSP we simply use an include directive
    ```jsp
    <body>
        <%@	include	file="/NewFile.jsp"%>
        The time is <%=new	Date() %>
    </body>
    ```
-   Tomcat embeds the response of the file
    ![](Images/88.png)

