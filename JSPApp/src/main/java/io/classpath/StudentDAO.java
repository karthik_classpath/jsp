package io.classpath;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.cj.protocol.Resultset;

public class StudentDAO {
	
	public static boolean	setStudent(String	sql,	Connection	connection) throws SQLException {
		Statement	stmnt	=	connection.createStatement();
		if(stmnt.executeUpdate(sql)>0)
				return	true;
		return	false;
	}
	
	public static	ResultSet	getStudent(Connection	con,String	sql) throws SQLException {
		Statement	statement	=	con.createStatement();
		return	(ResultSet)statement.executeQuery(sql);
	}
	
	public static boolean deleteStudent(Connection connection,String	sql) throws SQLException {
		Statement	statement	=	connection.createStatement();
		if(statement.executeUpdate(sql)>0)
			return	true;
		return	false;
	}
	
}

