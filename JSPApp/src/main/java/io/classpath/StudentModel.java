package io.classpath;

public class StudentModel {
	private	int	id;
	private	String	nameString;
	private	int age;
	private	String	streamString;
	private	int	yop;
	
	
	public StudentModel() {
		super();
	}

	public StudentModel(int id, String nameString, int age, String streamString, int yop) {
		super();
		this.id = id;
		this.nameString = nameString;
		this.age = age;
		this.streamString = streamString;
		this.yop = yop;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNameString() {
		return nameString;
	}
	public void setNameString(String nameString) {
		this.nameString = nameString;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getStreamString() {
		return streamString;
	}
	public void setStreamString(String streamString) {
		this.streamString = streamString;
	}
	public int getYop() {
		return yop;
	}
	public void setYop(int yop) {
		this.yop = yop;
	}
	
	@Override
	public String toString() {
		return "StudentModel [id=" + id + ", name=" + nameString + ", age=" + age + ", stream="
				+ streamString + ", Year of Passing=" + yop + "]";
	}
	
	
	
}
