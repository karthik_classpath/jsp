package io.classpath;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Register
 */
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		RequestDispatcher	requestDispatcher	=	request.getRequestDispatcher("form");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String	nameString = request.getParameter("name");
		int	age	=	Integer.parseInt(request.getParameter("age"));
		String	stream = request.getParameter("stream");
		int	yop	=	Integer.parseInt(request.getParameter("yop"));	
		
		String	sqlString	=	"insert into students(name,age,stream,yearOfPassing)values('"+nameString+"',"+age+",'"+stream+"',"+yop+")";
		Connection	connection	=	(Connection) getServletContext().getAttribute("connection");
		
		try {
			if(StudentDAO.setStudent(sqlString, connection)) {
				request.getRequestDispatcher("list").forward(request, response);
			}
			else {
				response.getWriter().write("Register operation failed please try again");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
