<%@page import="io.classpath.StudentModel"%>
<%@page import="io.classpath.StudentDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page	import="java.sql.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<style>
	table,
      
      td {
        padding: 10px;
        border: 1px solid black;
        border-collapse: collapse;
      }
</style>
</head>
<body>
	<form action="delete" method="post">
	<%
	Connection	connection	=	(Connection) getServletContext().getAttribute("connection");
	String	sqlString	=	"select * from students";
	ResultSet	rs	=	StudentDAO.getStudent(connection,sqlString);
	StudentModel	studentModel	=	new	StudentModel();
	%>
	<table >
		<tr	>
		    <td>ID</td>
		    <td>NAME</td>
		    <td>AGE</td>
			<td>STREAM</td>
			<td>YEAR OF PASSING</td>
			<td>TO DELETE</td>	
		</tr>
		
		<%while(rs.next()){
		%>
		
		<tr>
		    <td><%=rs.getInt(1) %></td>
		    <td><%=rs.getString(2) %></td>
		    <td><%=rs.getInt(3) %></td>
			<td><%=rs.getString(4) %></td>
			<td><%=rs.getInt(5) %></td>
			<td><input	type="submit"	name="delete" value=<%=rs.getInt(1) %>></td>	
		</tr>
		<%} %>
	</table>
	</form>	
		
</body>
</html>