<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
		String	userName = request.getParameter("name");
		if(userName!=null){
			session.setAttribute("sessionName", userName);
			//application.setAttribute("applicationName", userName);
			pageContext.setAttribute("applicationName", userName, pageContext.APPLICATION_SCOPE);
			pageContext.findAttribute("name");
		}
	%>
	<br>
	The username in the request object is :<%=userName %>
	<br>
	The username in the session object is :<%=session.getAttribute("sessionName") %>
	<br>
	The username in the application object is :<%=application.getAttribute("applicationName") %>
	<br>
	The username in the pageContext object is :<%=pageContext.getAttribute("pageContextName") %>
</body>
</html>