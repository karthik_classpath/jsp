<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<%!
	public	void	jspInit(){
		String	defaultUserString	=	getServletConfig().getInitParameter("defaultUser");
		getServletContext().setAttribute("default", defaultUserString);
	}
%>
<body>
	The default user from servlet <%=getServletConfig().getInitParameter("defaultUser") %>
	
	<br>The value in the servlet context is <%=getServletContext().getAttribute("default") %> 
</body>
</html>