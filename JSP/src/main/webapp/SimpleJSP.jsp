<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h3>Testing JSP</h3>
	<%!
		int	add(int i,int j){
			return	i+j;
		}
	%>
	<%
		int	i=1;
		int	j=3;
		int	k=i+j;
	%> 
	Value outside script of k is :<%=k %>
	<%
		k = add(100, 300);
	%>
	<br>Value of k from add : <%=k %>
	<br>
	
	<%
		for(i=0;i<4;i++){
	%>
		<br>New value of I =<%=i %>
	<%  
		}
	%>
</body>
</html>